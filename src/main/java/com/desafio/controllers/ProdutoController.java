package com.desafio.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.desafio.models.Produto;

@Controller
public class ProdutoController {
	
	static List<Produto> ListaProdutos = new ArrayList<Produto>();
	
	@RequestMapping(value="/cadastrar-produto", method=RequestMethod.POST)
	public String saveProduto(Produto data) {
		Produto p = new Produto();
		p.setNome(data.getNome());
		p.setIdadeMaxima(data.getIdadeMaxima());
		p.setIdadeMinima(data.getIdadeMinima());
		p.setTaxaJuros(data.getTaxaJuros());
		p.setValorMinimoPremio(data.getValorMinimoPremio());
	   
		ListaProdutos.add(p);
		return "redirect:/get-produtos";
	}

	@RequestMapping(value="/cadastrar-produto", method=RequestMethod.GET)
	public String form(){
		return "produto";
	}
	
	@RequestMapping("/get-produtos")
	public ModelAndView produto() {
		ModelAndView mv = new ModelAndView("lista");
		Iterable<Produto> produtos = ListaProdutos;
		mv.addObject("produto", produtos);
		return mv;
	}
	

	
}
