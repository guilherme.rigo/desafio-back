package com.desafio.controllers;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.desafio.gerenciador.response.Response;
import com.desafio.models.Produto;
import com.desafio.models.Simulacao;

@RestController
public class SimulacaoController {

	
	static List<Simulacao> ListaSimulacoes = new ArrayList<Simulacao>();
	ProdutoController pc = new ProdutoController();
	List<String> lista;
	Simulacao s =  new Simulacao();
	int numeroRequest = 0;
	

	@RequestMapping(value="/cadastrar-simulacao", method=RequestMethod.POST)
	public ResponseEntity<Response<Simulacao>> gerarSimulacao(@RequestBody Simulacao data){

			System.out.println("Requisicao");
			Response<Simulacao> response = new Response<Simulacao>();
			try {
				if(pc.ListaProdutos.size() > 0){
					DateTime dataSimulacao = new DateTime();
					s = data;
					Period period = new Period(dataSimulacao, s.getFimContratoEmprestimo());
					int numeroMeses = period.getYears() * 12 + period.getMonths();
					if(!CalculaValorPremio(s.getValorSegurado(), numeroMeses, CalculaIdade(s.getDataNascimento()))) {
						response.setErrors(lista);
						System.out.println(ResponseEntity.status(HttpStatus.BAD_REQUEST));
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
					}
					s.setDataSimulacao(dataSimulacao);
					s.setProdutoEscolhido(defineProduto(CalculaIdade(s.getDataNascimento()), pc.ListaProdutos));
					ListaSimulacoes.add(s);
					
					URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(s.getNomePessoa())
							.toUri();
					response.setData(s);
					System.out.println(ResponseEntity.created(location));
					return ResponseEntity.created(location).body(response);
				} else {
					lista = new ArrayList<String>();
					lista.add("Nenhum Produto encontrado!");
					response.setErrors(lista);
					System.out.println(ResponseEntity.status(HttpStatus.BAD_REQUEST));
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
				}
				
			} catch(NullPointerException e) {

				response.setErrors(lista);
				System.out.println(ResponseEntity.status(HttpStatus.BAD_REQUEST));
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}
	}
	
	public Produto defineProduto(int idade, List<Produto> p) {
		Produto produto = null;
		Produto prod = null;
		for(int linha=0; linha < p.size(); linha++) {
			prod = (Produto) p.get(linha);
			if(prod.getIdadeMaxima() > idade && prod.getIdadeMinima() < idade) {
				produto = prod;

				return produto;
			}
		}
		return produto;
	}
	
	public boolean CalculaValorPremio(BigDecimal valorSegurado, int numeroMeses, int idade) {
		Produto p = new Produto();
		try {
			if(numeroMeses >= 1 && numeroMeses <= 120) {
				p = defineProduto(idade, pc.ListaProdutos);
				BigDecimal taxaJuros = p.getTaxaJuros();
				BigDecimal nMeses = new BigDecimal(numeroMeses);
				BigDecimal valorTotalPremio = taxaJuros.divide(new BigDecimal(1000))
						.multiply(valorSegurado)
						.multiply(nMeses);
				if(valorTotalPremio.compareTo(p.getValorMinimoPremio()) == 1) {
					s.setValorTotalPremio(valorTotalPremio);
					return true;
				} else {
					s.setValorTotalPremio(p.getValorMinimoPremio());
					return true;
				}
			} else if(numeroMeses < 1){
				lista = new ArrayList<String>();
				lista.add("O numero mínimo de meses é 1!");
				return false;
			} else if(numeroMeses > 120) {
				lista = new ArrayList<String>();
				lista.add("O numero máximo de meses entre a Data Atual "
						+ "e a Data final do contrato é 120!");
				return false;
			}
			return true;
		} catch(NullPointerException e) {
			lista = new ArrayList<String>();
			lista.add("Nenhum Produto se encaixa na descrição!");
			return false;
		} 
	}
	
	private int CalculaIdade(DateTime dataNasc) {
		
		DateTime hojeDate = new DateTime();
		Period idade = new Period(dataNasc, hojeDate);
		return idade.getYears();
		
	}
	
	@RequestMapping("/simulacoes")
	public ResponseEntity<Response<List<Simulacao>>>  getSimulacoes() {
		Response<List<Simulacao>> response = new Response<List<Simulacao>>();

		response.setData(ListaSimulacoes);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(s.getNomePessoa())
				.toUri();
		return ResponseEntity.created(location).body(response);
	}
	
	@RequestMapping(value="/set-produto", method=RequestMethod.GET)
	public void setProdutos() {
		
		if(numeroRequest == 0 ) {
			Produto produto = new Produto();
			produto.setNome("Produto 1");
			produto.setIdadeMinima(18);
			produto.setIdadeMaxima(70);
			produto.setTaxaJuros(new BigDecimal(0.4500));
			produto.setValorMinimoPremio(new BigDecimal(5.00));
			pc.ListaProdutos.add(produto);
			Produto produto2 = new Produto();
			produto2.setNome("Produto 2");
			produto2.setIdadeMinima(71);
			produto2.setIdadeMaxima(75);
			produto2.setTaxaJuros(new BigDecimal(1.8472));
			produto2.setValorMinimoPremio(new BigDecimal(5.00));
			pc.ListaProdutos.add(produto2);
			numeroRequest++;
		}
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
	    return new WebMvcConfigurer() {
	        @Override
			public void addCorsMappings(CorsRegistry registry) {
	            registry.addMapping("/**")
	                    .allowedOrigins("*");
	        }
	    };
	}
	
}
