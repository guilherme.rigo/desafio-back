package com.desafio.models;

import java.math.BigDecimal;

import org.joda.time.DateTime;


public class Simulacao {

	private String nomePessoa;
	private String cpf;
	private BigDecimal valorSegurado;
	private String numeroContratoEmprestimo;
	private DateTime fimContratoEmprestimo;
	private DateTime dataNascimento;
	private Produto produtoEscolhido;
	private DateTime dataSimulacao;
	private BigDecimal valorTotalPremio;
	
	public String getNomePessoa() {
		return nomePessoa;
	}
	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public BigDecimal getValorSegurado() {
		return valorSegurado;
	}
	public void setValorSegurado(BigDecimal valorSegurado) {
		this.valorSegurado = valorSegurado;
	}
	public String getNumeroContratoEmprestimo() {
		return numeroContratoEmprestimo;
	}
	public void setNumeroContratoEmprestimo(String numeroContratoEmprestimo) {
		this.numeroContratoEmprestimo = numeroContratoEmprestimo;
	}
	public DateTime getFimContratoEmprestimo() {
		return fimContratoEmprestimo;
	}
	public void setFimContratoEmprestimo(DateTime fimContratoEmprestimo) {
		this.fimContratoEmprestimo = fimContratoEmprestimo;
	}
	public DateTime getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(DateTime dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Produto getProdutoEscolhido() {
		return produtoEscolhido;
	}
	public void setProdutoEscolhido(Produto produtoEscolhido) {
		this.produtoEscolhido = produtoEscolhido;
	}
	public DateTime getDataSimulacao() {
		return dataSimulacao;
	}
	public void setDataSimulacao(DateTime dataSimulacao) {
		this.dataSimulacao = dataSimulacao;
	}
	public BigDecimal getValorTotalPremio() {
		return valorTotalPremio;
	}
	public void setValorTotalPremio(BigDecimal valorTotalPremio) {
		this.valorTotalPremio = valorTotalPremio;
	}
	
}
