package com.desafio.models;

import java.math.BigDecimal;

public class Produto {
	
	private String nome;
	private Integer idadeMinima;
	private Integer idadeMaxima;
	private BigDecimal taxaJuros;
	private BigDecimal valorMinimoPremio;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdadeMinima() {
		return idadeMinima;
	}
	public void setIdadeMinima(Integer idadeMinima) {
		this.idadeMinima = idadeMinima;
	}
	public Integer getIdadeMaxima() {
		return idadeMaxima;
	}
	public void setIdadeMaxima(Integer idadeMaxima) {
		this.idadeMaxima = idadeMaxima;
	}
	public BigDecimal getTaxaJuros() {
		return taxaJuros;
	}
	public void setTaxaJuros(BigDecimal taxaJuros) {
		this.taxaJuros = taxaJuros;
	}
	public BigDecimal getValorMinimoPremio() {
		return valorMinimoPremio;
	}
	public void setValorMinimoPremio(BigDecimal valorMinimoPremio) {
		this.valorMinimoPremio = valorMinimoPremio;
	}

}
